package com.sadra.siakad.controller;

import com.sadra.siakad.payload.request.StudentRequest;
import com.sadra.siakad.payload.response.StudentResponse;
import com.sadra.siakad.service.StudentService;
import com.sadra.siakad.utils.DataResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/students")
@CrossOrigin(origins = "*")
public class StudentController {

    StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<StudentResponse> createStudent(@RequestBody StudentRequest request) {
        return new ResponseEntity<>(studentService.createStudent(request), HttpStatus.CREATED);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<DataResponse> getSingleStudent(@PathVariable(value = "userId") Long userId) {
        StudentResponse studentResponse = studentService.getDetailStudent(userId);
        DataResponse<StudentResponse> response = new DataResponse<>();
        response.setStatus(true);
        response.setMessages(null);
        response.setPayload(studentResponse);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{userId}")
    public ResponseEntity<StudentResponse> updateStudent(@RequestBody StudentRequest request,
                                                         @PathVariable(value = "userId") Long userId) {
        return new ResponseEntity<>(studentService.updateStudent(request, userId), HttpStatus.ACCEPTED);
    }
}
