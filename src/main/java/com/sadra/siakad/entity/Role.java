package com.sadra.siakad.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "roles")
@Data
@EntityListeners(AuditingEntityListener.class)
public class Role {
        @Id
        @GeneratedValue(generator = "sequence-generator")
        @GenericGenerator(name = "sequence-generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
                        @org.hibernate.annotations.Parameter(name = "sequence_name", value = "roles_id_seq")
        })
        private Long id;

        @CreatedDate
        @Column(name = "created_date", nullable = false)
        private Date createdDate;

        @LastModifiedDate
        @Column(name = "updated_date")
        private Date updatedDate;

        @Column(name = "deleted_date")
        private Date deletedDate;

        @Column(name = "name")
        private String name;

        @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "roles")
        private Set<User> users = new HashSet<>();
}
