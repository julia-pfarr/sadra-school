package com.sadra.siakad.utils;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DataResponse<T> {
    private boolean status;
    private List<String> messages = new ArrayList<>();
    private T payload;
}
