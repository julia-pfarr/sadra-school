package com.sadra.siakad.utils;

import org.springframework.http.HttpStatus;

public class ErrorException extends RuntimeException{
    private HttpStatus status;
    private String message;

    public ErrorException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public ErrorException(String message, HttpStatus status, String message1) {
        super(message);
        this.status = status;
        this.message = message1;
    }

    public HttpStatus getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}