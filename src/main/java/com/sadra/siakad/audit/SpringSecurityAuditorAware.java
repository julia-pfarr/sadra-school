package com.sadra.siakad.audit;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class SpringSecurityAuditorAware implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable("Admin Sadra").filter(s -> !s.isEmpty());
    }
}
