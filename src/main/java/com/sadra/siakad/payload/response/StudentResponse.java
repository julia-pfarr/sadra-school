package com.sadra.siakad.payload.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentResponse {
    private Long userId;
    private String nim;
    private String username;
    private String email;
    private String fullName;
    private String motherName;
    private String fatherName;
    private String alamat;
    private String kelurahan;
    private String kecamatan;
    private String kabupaten;
    private String provinsi;
    private String kodePos;
    private String photoUrl;
}
