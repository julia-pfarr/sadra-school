package com.sadra.siakad.payload.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentRequest {
    private String nim;
    private String email;
    private String username;
    private String fullName;
    private String fatherName;
    private String motherName;
    private String password;
    private String alamat;
    private String kelurahan;
    private String kecamatan;
    private String kabupaten;
    private String provinsi;
    private String kodePos;
    private String photoUrl;
}
