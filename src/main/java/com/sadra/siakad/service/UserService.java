package com.sadra.siakad.service;

import com.sadra.siakad.payload.request.UserRequest;
import com.sadra.siakad.payload.response.PageUserResponse;
import com.sadra.siakad.payload.response.UserResponse;

public interface UserService {
    UserResponse createUser(UserRequest request);
    UserResponse currentUser(String email);
    PageUserResponse getAllUsers(String email,
                                 String username,
                                 String nim,
                                 int pageNo,
                                 int pageSize,
                                 String sortBy,
                                 String sortDir);
}
