package com.sadra.siakad.service;

import com.sadra.siakad.payload.request.StudentRequest;
import com.sadra.siakad.payload.response.StudentResponse;

public interface StudentService {
    StudentResponse createStudent(StudentRequest request);
    StudentResponse getDetailStudent(Long studentId);
    StudentResponse updateStudent(StudentRequest request, Long userId);
}
