package com.sadra.siakad.repository;

import com.sadra.siakad.entity.File;
import org.springframework.data.repository.CrudRepository;

public interface FileRepository extends CrudRepository<File, Long> {
}
