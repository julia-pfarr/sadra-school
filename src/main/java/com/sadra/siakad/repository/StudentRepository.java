package com.sadra.siakad.repository;

import com.sadra.siakad.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
