package com.sadra.siakad.repository;

import com.sadra.siakad.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsernameOrEmailOrNim(String username, String email, String nim);
    Optional<User> findByEmail(String email);

    String queryUserFilter = "select u.* from users u " +
            "where (:nim = '' OR (u.nim ILIKE %:nim%)) AND " +
            "(:username = '' OR (u.username ILIKE %:username%)) AND " +
            "(:email = '' OR (u.email ILIKE %:email%))" +
            "Order BY u.created_date desc";
    @Query(value = queryUserFilter, nativeQuery = true)
    Page<User> getUserFilter(@Param("nim") String nim,
                             @Param("username") String username,
                             @Param("email") String email,
                             Pageable pageable);
}
