create table addresses(
    id bigserial not null
       constraint address_pk
        primary key,
    alamat varchar (255),
    kelurahan varchar(255),
    kecamatan varchar(255),
    kabupaten varchar (255),
    provinsi varchar (255),
    kode_pos varchar(10),
    user_id bigint not null
        constraint students_users_id_fk
            references users,
    created_date timestamptz default current_timestamp not null,
    updated_date timestamptz not null,
    deleted_date timestamptz,
    created_by VARCHAR(255),
    updated_by VARCHAR(255)
);