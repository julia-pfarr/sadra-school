create table files(
    id bigserial not null
       constraint files_pk
        primary key,
    name varchar (255),
    file_url varchar(255),
    created_date timestamptz default current_timestamp not null,
    updated_date timestamptz not null,
    deleted_date timestamptz,
    created_by VARCHAR(255),
    updated_by VARCHAR(255)
);